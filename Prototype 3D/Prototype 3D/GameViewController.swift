//
//  GameViewController.swift
//  Prototype 3D
//
//  Created by Carlos Villamizar on 10/19/20.
//  Copyright © 2020 Carlos Villamizar. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {
    
    var textnode: SCNNode!
    var text: SCNText!
    
    var arrowNode: SCNNode!
    var arrow: SCNBox!
    
    var textTailNode: SCNNode!
    var textTail: SCNText!
    
    var arrowTailNode: SCNNode!
    var arrowTail: SCNBox!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // create and add a camera to the scene
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        scene.rootNode.addChildNode(cameraNode)
        
        // place the camera
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 20)
        
        // create and add a light to the scene
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        // create and add an ambient light to the scene
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        // retrieve the element node
        textnode = scene.rootNode.childNode(withName: "TextName", recursively: true)
        text = textnode.geometry as? SCNText
        
        arrowNode = scene.rootNode.childNode(withName: "LineBox", recursively: true)
        arrow = arrowNode.geometry as? SCNBox
        
        textTailNode = scene.rootNode.childNode(withName: "TailText", recursively: true)
        textTail = textnode.geometry as? SCNText
        
        arrowTailNode = scene.rootNode.childNode(withName: "TailLineBox", recursively: true)
        arrowTail = arrowNode.geometry as? SCNBox
        
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        // set the scene to the view
        scnView.scene = scene
        
        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        
        // show statistics such as fps and timing information
        scnView.showsStatistics = true
        
        // configure the view
        scnView.backgroundColor = UIColor.black
        
        // add a tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scnView.addGestureRecognizer(tapGesture)
        
        
    }
    
    @objc
    func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        // check what nodes are tapped
        let p = gestureRecognize.location(in: scnView)
        let hitResults = scnView.hitTest(p, options: [:])
        // check that we clicked on at least one object
        if hitResults.count > 0 {
            // retrieved the first clicked object
            let result = hitResults[0]
            
            // get its material
            let material = result.node.geometry!.firstMaterial!
            
            print(material.name)
            
            if material.name == "NouseShip" {
                textnode.isHidden = false
                arrowNode.isHidden = false
                
                textTailNode.isHidden = true
                arrowTailNode.isHidden = true
            }else if material.name == "TailShip" {
                
                print("Aqui")
                textnode.isHidden = true
                arrowNode.isHidden = true
                
                textTailNode.isHidden = false
                arrowTailNode.isHidden = false
            }else{
                textnode.isHidden = true
                arrowNode.isHidden = true
                textTailNode.isHidden = true
                arrowTailNode.isHidden = true
            }
            
            // highlight it
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.5
            
            // on completion - unhighlight
            SCNTransaction.completionBlock = {
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 0.5
                
                material.emission.contents = UIColor.black
                
                SCNTransaction.commit()
            }
            
            material.emission.contents = UIColor.red
            
            SCNTransaction.commit()
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

}
